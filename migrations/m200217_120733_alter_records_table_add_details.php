<?php

use yii\db\Migration;

/**
 * Class m200217_120733_alter_records_table_add_details
 */
class m200217_120733_alter_records_table_add_details extends Migration
{
  /**
   * {@inheritdoc}
   */
  public function safeUp()
  {
    $this->addColumn('records', 'details', 'varchar(1024) default null');
  }

  /**
   * {@inheritdoc}
   */
  public function safeDown()
  {
    $this->dropColumn('records', 'details');
  }

}
