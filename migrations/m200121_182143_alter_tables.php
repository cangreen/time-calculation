<?php

use yii\db\Migration;

/**
 * Class m200121_182143_alter_tables
 */
class m200121_182143_alter_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('objects', 'object_number', 'integer not null');

        $this->addColumn('employees', 'table_number', 'integer not null');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('objects', 'object_number');

        $this->dropColumn('employees', 'table_number');
    }
}
