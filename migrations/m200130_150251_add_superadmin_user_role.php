<?php

use yii\db\Migration;

/**
 * Class m200130_150251_add_superadmin_user_role
 */
class m200130_150251_add_superadmin_user_role extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
      $am = Yii::$app->authManager;
      $superAdmin = $am->createRole('superadmin');
      $am->add($superAdmin);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200130_150251_add_superadmin_user_role cannot be reverted.\n";

        return false;
    }

}
