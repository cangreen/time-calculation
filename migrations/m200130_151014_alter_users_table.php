<?php

use yii\db\Migration;

/**
 * Class m200130_151014_alter_users_table
 */
class m200130_151014_alter_users_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('users', 'hub_id', $this->integer());

        $this->addForeignKey(
            'users_hubs_fk',
            'users',
            'hub_id',
            'transport_hubs',
            'id',
            'SET NULL'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('users_hubs_fk', 'users');
        $this->dropColumn('users', 'hub_id');
    }

}
