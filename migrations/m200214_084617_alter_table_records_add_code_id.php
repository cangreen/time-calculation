<?php

use yii\db\Migration;

/**
 * Class m200214_084617_alter_table_records_add_code_id
 */
class m200214_084617_alter_table_records_add_code_id extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
      $this->addColumn('records', 'table_code_id', 'integer default null');

      $this->addForeignKey(
        'records_table_codes_fk',
        'records',
        'table_code_id',
        'table_codes',
        'id',
        'SET NULL'
      );
    }


    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
       $this->dropForeignKey('records_table_codes_fk', 'records');

       $this->dropColumn('records', 'table_code_id');
    }

}
