<?php

use yii\db\Migration;

/**
 * Class m200202_173210_alter_objects_table
 */
class m200202_173210_alter_objects_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('objects', 'hub_id', $this->integer());

        $this->addForeignKey(
            'objects_hubs_fk',
            'objects',
            'hub_id',
            'transport_hubs',
            'id',
            'SET NULL'
        );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('objects_hubs_fk', 'objects');
        $this->dropColumn('objects', 'hub_id');
    }

}
