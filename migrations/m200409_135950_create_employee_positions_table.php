<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%employee_positions}}`.
 */
class m200409_135950_create_employee_positions_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%employee_positions}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(64)->notNull()->unique()
        ]);

        $this->addColumn('employees', 'position_id', 'integer default null');

        $this->addForeignKey(
            'employees_position_fk',
            'employees',
            'position_id',
            'employee_positions',
            'id',
            'SET NULL'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('employees_position_fk', 'employees');

        $this->dropColumn('employees', 'position_id');

        $this->dropTable('{{%employee_positions}}');
    }
}
