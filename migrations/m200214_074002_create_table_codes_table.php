<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%table_codes}}`.
 */
class m200214_074002_create_table_codes_table extends Migration
{
  /**
   * {@inheritdoc}
   */
  public function safeUp()
  {
    $codes = [
      ['НН', 'Неявки по невыясненной причине', false, false],
      ['В', 'Выходные дни (еженедельный отпуск) и нерабочие праздничные дни', false, false],
      ['ОТ', 'Ежегодный дополнительный оплачиваемый отпуск', true, false],
      ['ОЖ', 'Отпуск по уходу за ребенком до достижения им возраста трех лет', true, false],
      ['Р', 'Отпуск по беременности и родам (отпуск в связи с усыновлением новорожденного ребенка)', true, false],
      ['ПР', 'Прогулы (отсутствие на рабочем месте без уважительных причин в течение времени, установленного законодательством)', true, true],
      ['Б', 'Временная нетрудоспособность (кроме случаев, предусмотренных кодом «Т») с назначением пособия, согласно законодательству', true, true],
      ['ДО', 'Отпуск без сохранения заработной платы, предоставленный работнику по разрешению работодателя',
        true, true],
    ];

    $this->createTable('{{%table_codes}}', [
      'id' => $this->primaryKey(),
      'code' => $this->string(8)->notNull(),
      'details' => $this->string(256),
      'is_periodic' => $this->boolean()->defaultValue(false),
      'admin_only' => $this->boolean()->defaultValue(false),
    ]);

    foreach ($codes as $code) {
      $this->insert('table_codes', [
        'code' => $code[0],
        'details' => $code[1],
        'is_periodic' => $code[2],
        'admin_only' => $code[3],
      ]);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function safeDown()
  {
    $this->dropTable('{{%table_codes}}');
  }
}
