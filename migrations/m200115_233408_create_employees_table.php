<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%employees}}`.
 */
class m200115_233408_create_employees_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%employees}}', [
            'id' => $this->primaryKey(),
            'object_id' => $this->integer(),
            'full_name' => $this->string(128),
            'is_active' => $this->boolean()
        ]);

        $this->addForeignKey(
            'employee_object_fk',
            'employees',
            'object_id',
            'objects',
            'id',
            'SET NULL'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('employee_object_fk', 'employees');

        $this->dropTable('{{%employees}}');
    }
}
