<?php

use yii\db\Migration;

/**
 * Class m200413_141548_alter_records_table_add_position_id
 */
class m200413_141548_alter_records_table_add_position_id extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('records','position_id', 'integer default null');

        $this->addForeignKey(
            'records_positions_fk',
            'records',
            'position_id',
            'employee_positions',
            'id',
            'SET NULL'
        );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('records_positions_fk', 'records');

        $this->dropColumn('records', 'position_id');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200413_141548_alter_records_table_add_position_id cannot be reverted.\n";

        return false;
    }
    */
}
