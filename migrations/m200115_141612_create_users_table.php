<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%users}}`.
 */
class m200115_141612_create_users_table extends Migration
{
  /**
   * {@inheritdoc}
   */
  public function safeUp()
  {
    $this->createTable('users', [
      'id' => $this->primaryKey(),
      'username' => $this->string()->notNull()->unique(),
      'password_hash' => $this->string()->notNull(),
      'email' => $this->string()->notNull()->unique(),
      'object_id' => $this->integer(),
    ]);

    $this->addForeignKey(
      'user_object_fk',
      'users',
      'object_id',
      'objects',
      'id',
      'SET NULL'
    );
  }

  /**
   * {@inheritdoc}
   */
  public function safeDown()
  {
    $this->dropForeignKey('user_object_fk', 'users');
    
    $this->dropTable('{{%users}}');
  }
}
