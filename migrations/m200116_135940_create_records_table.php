<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%records}}`.
 */
class m200116_135940_create_records_table extends Migration
{
  /**
   * {@inheritdoc}
   */
  public function safeUp()
  {
    $this->createTable('{{%records}}', [
      'id' => $this->primaryKey(),
      'created_at' => $this->integer()->notNull(),
      'object_id' => $this->integer()->notNull(),
      'employee_id' => $this->integer()->notNull(),
      'type' => $this->smallInteger()->notNull(),
      'start' => $this->integer()->notNull(),
      'end' => $this->integer()->notNull(),
    ]);

    $this->addForeignKey(
      'record_employee_fk',
      'records',
      'employee_id',
      'employees',
      'id',
      'CASCADE'
    );
    $this->addForeignKey(
      'record_object_fk',
      'records',
      'object_id',
      'objects',
      'id',
      'CASCADE'
    );
  }

  /**
   * {@inheritdoc}
   */
  public function safeDown()
  {
    $this->dropForeignKey('record_employee_fk', 'records');
    $this->dropForeignKey('record_object_fk', 'records');
    $this->dropTable('{{%records}}');
  }
}
