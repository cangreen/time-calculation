<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%transport_hubs}}`.
 */
class m200127_141526_create_transport_hubs_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%transport_hubs}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(32)->notNull()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%transport_hubs}}');
    }
}
