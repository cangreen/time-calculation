"use strict";

const $elem = $('#records-position_id');
const $elemSingle = $('#records-position_id_selected');


$elem.on('depdrop:change', function (event, id, value, count) {
    if(count === 1) {
        const value = event.target[1].value;
        const text = event.target[1].innerText;
        $elem.parent().parent().hide();
        $elemSingle.parent().parent().show();
        $elemSingle.prop('name', event.target.name);
        $elemSingle.find('option').prop('value', value).html(text);
    } else {
        $elem.parent().parent().show();
        $elemSingle.parent().parent().hide();
    }
});