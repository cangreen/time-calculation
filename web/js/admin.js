"use strict";

const isRecordManager = $('.record-day_manager').length !== 0;

$('#users-isadmin').change(function (e) {
  if (e.target.checked) {
    $('#createuser-object_id').attr('disabled', true);
  } else {
    $('#createuser-object_id').attr('disabled', false);
  }
});

$('#records-table_code_id').change(function (e) {
  let value = '';
  $(e.target).find('option').each(function () {
    if ($(this).get(0).selected) {
      value = $(this).val();
    }
  });

  if (value !== '' && !isRecordManager) {
    $('.record-period').show();
    $('.record-day').hide();
  } else if (value !== '' && isRecordManager) {
    const date = new Date();
    const dayStart = `${date.getUTCDate()}-${('0' + (date.getMonth() + 1)).slice()}-${date.getFullYear()} 00:00`;
    const dayEnd = `${date.getUTCDate()}-${('0' + (date.getMonth() + 1)).slice()}-${date.getFullYear()} 23:59`;
    const dayStartUnix = new Date(`${date.getFullYear()}.${('0' + (date.getMonth() + 1)).slice()}.${date.getDate()} 00:00`).getTime();
    const dayEndUnix = new Date(`${date.getFullYear()}.${('0' + (date.getMonth() + 1)).slice()}.${date.getDate()} 23:59`).getTime();
    $('#records-start-disp').val(dayStart).attr('disabled', true);
    $('#records-end-disp').val(dayEnd).attr('disabled', true);
    $('#records-start').val(dayStartUnix / 1000);
    $('#records-end').val(dayEndUnix / 1000);
    $('#records-start-disp').prev().prev().hide();
    $('#records-start-disp').prev().hide();
    $('#records-end-disp').prev().hide();
    $('#records-end-disp').prev().prev().hide();

  } else if (value === '' && isRecordManager) {
    $('#records-start-disp').val('').attr('disabled', false);
    $('#records-end-disp').val('').attr('disabled', false);
    $('#records-start-disp').prev().prev().show();
    $('#records-start-disp').prev().show();
    $('#records-end-disp').prev().show();
    $('#records-end-disp').prev().prev().show();
  } else {
    $('.record-period').hide();
    $('.record-day').show();
  }
});

