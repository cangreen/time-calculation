<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "table_codes".
 *
 * @property int $id
 * @property string $code
 * @property string|null $details
 * @property int|null $is_periodic
 * @property int|null $admin_only
 */
class TableCodes extends \yii\db\ActiveRecord
{
  /**
   * {@inheritdoc}
   */
  public static function tableName()
  {
    return 'table_codes';
  }

  /**
   * {@inheritdoc}
   */
  public function rules()
  {
    return [
      [['code'], 'required'],
      [['code'], 'string', 'max' => 8],
      [['is_periodic', 'admin_only'], 'integer'],
      [['details'], 'string', 'max' => 256],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function attributeLabels()
  {
    return [
      'id' => 'ID',
      'code' => 'Код',
      'details' => 'Расшифровка',
      'is_periodic' => 'Период',
      'admin_only' => 'Только админ',
    ];
  }
}
