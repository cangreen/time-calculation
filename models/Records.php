<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "records".
 *
 * @property int $id
 * @property int $employee_id
 * @property int $object_id
 * @property int $start
 * @property int $end
 * @property int $start_period
 * @property int $end_period
 * @property int $created_at
 * @property int $type
 * @property string|null $details
 * @property int|null $table_code_id
 * @property Employees $employee
 * @property Objects $object
 * @property EmployeePositions $position
 */
class Records extends \yii\db\ActiveRecord
{

  public $start_period = null;

  public $end_period = null;

  const SCENARIO_TABLE_CODE = 'table_code';

  const TYPE_WORK = 1;
  const TYPE_PART_WORK = 2;
  const TYPE_MOBILE = 3;

  public function scenarios()
  {
    $scenarios = parent::scenarios();
    $scenarios[self::SCENARIO_TABLE_CODE] = [
      'employee_id', 'object_id', 'start_period', 'end_period', 'created_at', 'type', 'table_code_id', 'details'
    ];
    return $scenarios;
  }

  /**
   * {@inheritdoc}
   */
  public static function tableName()
  {
    return 'records';
  }

  /**
   * {@inheritdoc}
   */
  public function rules()
  {
    return [
      [['employee_id', 'object_id', 'start', 'end', 'created_at', 'type'], 'required'],
      [['employee_id', 'object_id', 'table_code_id', 'start', 'end', 'start_period', 'end_period', 'type', 'created_at'], 'integer'],
      [['start'], 'checkStartDayIsCreateDay', 'skipOnError' => false, 'skipOnEmpty' => false],
      [['end'], 'checkSameDay', 'skipOnError' => false, 'skipOnEmpty' => false],
      [['end'], 'checkEndDate', 'skipOnError' => false, 'skipOnEmpty' => false],
      [['type'], 'checkType', 'skipOnError' => false, 'skipOnEmpty' => false],
      [['type'], 'checkTypeInterval', 'skipOnError' => false, 'skipOnEmpty' => false],
      [['details'], 'string', 'max' => 1024],
      [['employee_id'], 'exist', 'skipOnError' => true, 'targetClass' => Employees::className(), 'targetAttribute' => ['employee_id' => 'id']],
      [['object_id'], 'exist', 'skipOnError' => true, 'targetClass' => Objects::className(), 'targetAttribute' => ['object_id' => 'id']],
      [['table_code_id'], 'exist', 'skipOnError' => true, 'targetClass' => TableCodes::className(), 'targetAttribute' => ['table_code_id' => 'id']],
      [['position_id'], 'exist', 'skipOnError' => true, 'targetClass' => EmployeePositions::className(), 'targetAttribute' => ['position_id' => 'id']],
      [['table_code_id'], 'checkIfWorkRecordExists', 'skipOnError' => false, 'skipOnEmpty' => false],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function attributeLabels()
  {
    return [
      'id' => 'ID',
      'employee_id' => 'ФИО соотрудника',
      'position_id' => 'Должность сотрудника',
      'positionName' => 'Должность сотрудника',
      'employeeFullName' => 'ФИО соотрудника',
      'employeeTableNumber' => 'Таб.№ сотрудника',
      'object_id' => 'Объект',
      'objectName' => 'Объект',
      'objectNumber' => 'Номер объекта',
      'start' => 'Дата/время начала',
      'end' => 'Дата/время окончания',
      'start_period' => 'Начало периода',
      'end_period' => 'Окончание периода',
      'type' => 'Тип записи',
      'created_at' => 'Дата/время создания',
      'table_code_id' => 'Код учёта',
      'code' => 'Код учёта',
      'details' => 'Примечание',
    ];
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getEmployee()
  {
    return $this->hasOne(Employees::className(), ['id' => 'employee_id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getPosition()
  {
    return $this->hasOne(EmployeePositions::className(), ['id' => 'position_id']);
  }

  /**
   * @return string|null
   */
  public function getPositionName()
  {
    return !is_null($this->position) ? $this->position->name : null;
  }

  /**
   * @return string
   */
  public function getEmployeeFullName()
  {
    return $this->employee->full_name;
  }

  /**
   * @return string
   */
  public function getEmployeeTableNumber()
  {
    return $this->employee->table_number;
  }


  /**
   * @return \yii\db\ActiveQuery
   */
  public function getObject()
  {
    return $this->hasOne(Objects::className(), ['id' => 'object_id']);
  }

  /**
   * @return string
   */
  public function getObjectName()
  {
    return $this->object->name;
  }

  /**
   * @return string
   */
  public function getObjectNumber()
  {
    return $this->object->object_number;
  }

  /**
   * Gets query for [[TableCode]].
   *
   * @return \yii\db\ActiveQuery
   */
  public function getTableCode()
  {
    return $this->hasOne(TableCodes::className(), ['id' => 'table_code_id']);
  }

  public function getCode()
  {
    return is_null($this->table_code_id) ? null : $this->tableCode->code;
  }

  /**
   * Проверка, что дата окончания тот же день, что и дата начала
   * @param $attribute
   * @return bool
   */
  public function checkSameDay($attribute)
  {
    $day = $this->getDay('start');

    if ($this->$attribute < $day['dayStart'] || $this->$attribute > $day['dayEnd']) {
      $this->addError($attribute, 'Дата начала и окончания должны быть в пределах одного дня');
      return false;
    }
    return true;
  }


  /**
   * Проверка, что дата окончания не раньше даты начала
   * @param $attribute
   * @return bool
   */
  public function checkEndDate($attribute)
  {
    if ($this->$attribute < $this->start) {
      $this->addError($attribute, 'Дата окончания не может быть раньше даты начала');
      return false;
    }
  }

  public function checkType($attribute)
  {
    $day = $this->getDay('start');
    $query = Records::find()->where([
      'employee_id' => $this->employee_id,
      'type' => $this->$attribute
    ])->andWhere([
      'between',
      'start',
      $day['dayStart'],
      $day['dayEnd']
    ]);

    if (Yii::$app->controller->action->id === 'update') {
      $query->andWhere([
        '!=',
        'id',
        $this->id
      ]);
    }
    $records = $query->all();

    if (count($records) !== 0) {
      $this->addError($attribute, 'Не может быть более одной записи одного типа одного сотрудника в день');
      return false;
    }
  }

  public function checkStartDayIsCreateDay($attribute)
  {
    if (Yii::$app->user->can('manager')) {
      $day = $this->getDay('created_at');
      if ($this->$attribute < $day['dayStart'] || $this->$attribute > $day['dayEnd']) {
        $this->addError($attribute, 'Доступна запись только в текущий день');
        return false;
      }
    }
  }

  public function checkIfWorkRecordExists($attribute)
  {
    $records = Records::find()->where([
      'between',
      $attribute,
      $this->start,
      $this->end,
    ])->all();

    if (count($records) !== 0) {
      $this->addError($attribute, 'Уже есть запись в один из дней периода');
      return false;
    }
  }

  public function checkTypeInterval($attribute)
  {

    if ((int)$this->$attribute === Records::TYPE_WORK || (int)$this->$attribute === Records::TYPE_PART_WORK) {

      $query = Records::find()->where([
        'between',
        'start',
        $this->start,
        $this->end
      ])->orWhere([
        'between',
        'end',
        $this->start,
        $this->end
      ]);

      $query->andFilterWhere([
        'employee_id' => $this->employee_id,
        'type' => [Records::TYPE_WORK, Records::TYPE_PART_WORK]
      ]);

      if (Yii::$app->controller->action->id === 'update') {
        $query->andFilterWhere([
          '!=',
          'id',
          $this->id
        ]);
      }

      if (count($query->all()) !== 0) {
        $this->addError($attribute, 'В указанном интервале уже есть запись типа Рабочий/подработка ');
        return false;
      }
    }

  }

  /**  Форматирование интервала между датами в часы и минуты
   * @return string
   */

  public function formatSecondsInterval()
  {
    $interval = $this->end - $this->start;
    $hours = 0;
    $minutes = 0;
    if ($interval > 3600) {
      $hours = floor($interval / 3600);
      $interval = $interval % 3600;
    }
    $minutes += floor($interval / 60);
    $result = '';

    if ((int)$hours !== 0) {
      $result .= "{$hours}ч ";
    }
    if ((int)$minutes !== 0) {
      $result .= "{$minutes}м ";
    }
    return $result;
  }

  /**
   * Находим timestamp начала и конца дня для сравнения
   * @param $attribute
   * @return array
   * @throws \yii\base\InvalidConfigException
   */
  public function getDay($attribute)
  {
    $result = [];
    $day = Yii::$app->formatter->asDate($this->$attribute);
    $result['dayStart'] = Yii::$app->formatter->asTimestamp($day . ' 00:00:00');
    $result['dayEnd'] = Yii::$app->formatter->asTimestamp($day . ' 23:59:59');
    return $result;
  }

  public function getDaysOfPeriod($start, $end)
  {
    $result = [];
    for ($currentDate = $start; $currentDate <= $end; $currentDate += 86400) {
      $result[] = $currentDate;
    }
    return $result;
  }

}

