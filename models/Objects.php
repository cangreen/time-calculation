<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "objects".
 *
 * @property int $id
 * @property string|null $name
 * @property integer $object_number
 * @property int|null $hub_id
 *
 * @property Users[] $users
 */
class Objects extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'objects';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 128],
            [['name', 'object_number'], 'required'],
            [['hub_id'], 'exist', 'skipOnError' => true, 'targetClass' => TransportHubs::className(), 'targetAttribute' => ['hub_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'object_number' => 'Номер объекта',
            'hub_id' => 'ТУ',
            'hubName' => 'ТУ',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(Users::className(), ['object_id' => 'id']);
    }

    /**
     * Gets query for [[Hub]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getHub()
    {
        return $this->hasOne(TransportHubs::className(), ['id' => 'hub_id']);
    }

    /**
     * @return mixed
     */
    public function getHubName()
    {
        return $this->hub->name;
    }

}
