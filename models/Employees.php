<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "employees".
 *
 * @property int $id
 * @property int|null $object_id
 * @property int|null $position_id
 * @property int $table_number
 * @property string|null $full_name
 * @property int|null $is_active
 *
 * @property Objects $object
 * @property Objects $position
 * @property string|null $positionName
 */
class Employees extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'employees';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['object_id', 'position_id', 'is_active', 'table_number'], 'integer'],
            [['full_name', 'table_number', 'is_active'], 'required'],
            [['full_name'], 'string', 'max' => 128],
            [['object_id'], 'exist', 'skipOnError' => true, 'targetClass' => Objects::className(), 'targetAttribute' => ['object_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'table_number' => 'Табельный номер',
            'object_id' => 'Объект',
            'objectName' => 'Объект',
            'full_name' => 'ФИО',
            'is_active' => 'Активность',
            'position_id' => 'Должность',
            'positionName' => 'Должность',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getObject()
    {
        return $this->hasOne(Objects::className(), ['id' => 'object_id']);
    }

    /**
     * @return string|null
     */

    public function getObjectName()
    {
        return !is_null($this->object) ? $this->object->name : null;
    }

    /**
     * Gets query for [[Position]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPosition()
    {
        return $this->hasOne(EmployeePositions::className(), ['id' => 'position_id']);
    }

    /**
     * @return string|null
     */
    public function getPositionName()
    {
        return !is_null($this->position) ? $this->position->name : null;
    }
}
