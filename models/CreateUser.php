<?php


namespace app\models;


use yii\base\Model;

class CreateUser extends Model
{
    public $username;
    public $email;
    public $password;
    public $isAdmin = false;
    public $object_id = false;
    public $hub_id = false;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['username', 'password', 'email'], 'required'],
            ['username', 'unique', 'targetClass' => Users::className()],
            ['password', 'string', 'min' => 6],
            ['email', 'email'],
            ['email', 'unique', 'targetClass' => Users::className()],
            [['object_id', 'hub_id'], 'safe'],
            [['object_id', 'hub_id'], 'default', 'value' => null],
            [['isAdmin'], 'boolean'],
            [['username', 'password', 'email'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Имя пользователя',
            'password' => 'Пароль',
            'email' => 'Email',
            'object_id' => 'Объект',
            'hub_id' => 'ТУ',
            'objectName' => 'Объект',
            'isAdmin' => 'Администратор?',
        ];
    }

    public function userCreate()
    {
        if (!$this->validate()) {
            return false;
        }
        $user = new Users();
        $user->username = $this->username;
        $user->isAdmin = $this->isAdmin;
        $user->email = $this->email;
        if ($this->object_id) {
            $user->object_id = $this->object_id;
        }

        if ($this->hub_id) {
            $user->hub_id = $this->hub_id;
        }

        $user->setPassword($this->password);
        return $user->save();
    }

    /**
     * @return TransportHubs|null
     */
    public function getHub()
    {
        return TransportHubs::findOne([$this->hub_id]);
    }

    /**
     * @return string|null
     */
    public function getHubName()
    {
        if (!empty($this->hub))
            return $this->hub->name;
    }


}