<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "users".
 *
 * @property int $id
 * @property string $username
 * @property string $password_hash
 * @property string $email
 * @property int|null $object_id
 * @property int|null $hub_id
 * @property boolean $isAdmin
 *
 * @property Objects $object
 */
class Users extends \yii\db\ActiveRecord
{
    public $isAdmin = false;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'users';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['username', 'password_hash', 'email'], 'required'],
            [['object_id', 'hub_id'], 'safe'],
            [['object_id', 'hub_id'], 'default', 'value' => null],
            [['isAdmin'], 'boolean'],
            [['username', 'password_hash', 'email'], 'string', 'max' => 255],
            [['username'], 'unique'],
            [['email'], 'unique'],
            ['email', 'email'],
            [['object_id'], 'exist', 'skipOnError' => true, 'targetClass' => Objects::className(), 'targetAttribute' => ['object_id' => 'id']],
            [['hub_id'], 'exist', 'skipOnError' => true, 'targetClass' => TransportHubs::className(), 'targetAttribute' =>
                ['hub_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Имя пользователя',
            'password_hash' => 'Password Hash',
            'password' => 'Пароль',
            'email' => 'Email',
            'object_id' => 'Объект',
            'hub_id' => 'ТУ',
            'objectName' => 'Объект',
            'isAdmin' => 'Администратор?',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getObject()
    {
        return $this->hasOne(Objects::className(), ['id' => 'object_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHub()
    {
        return $this->hasOne(TransportHubs::className(), ['id' => 'hub_id']);
    }

    /**
     * @return string|null
     */
    public function getObjectName()
    {
        if (!empty($this->object))
            return $this->object->name;
    }

    /**
     * @return string|null
     */
    public function getHubName()
    {
        if (!empty($this->hub))
            return $this->hub->name;
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id]);
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username]);
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
        return true;
    }

    public function afterSave($insert, $changedAttributes)
    {
        $am = Yii::$app->authManager;

        if ($insert) {
            if ($this->isAdmin && !is_null($this->hub_id)) {
                $role = $am->getRole('admin');
            } else if ($this->isAdmin && is_null($this->hub_id)) {
                $role = $am->getRole('superadmin');
            } else {
                $role = $am->getRole('manager');
            }

            $am->assign($role, $this->id);
        }

        if (!$insert && array_key_exists('hub_id', $changedAttributes) && $changedAttributes['hub_id'] !== (int)$this->hub_id) {
            if (is_null($this->hub_id)) {
                $oldRole = $am->getRole('admin');
                $role = $am->getRole('superadmin');
            } else {
                $oldRole = $am->getRole('superadmin');
                $role = $am->getRole('admin');
            }
            $am->revoke($oldRole, $this->id);
            $am->assign($role, $this->id);
        }
    }
}
