<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Employees;

/**
 * EmployeesSearch represents the model behind the search form of `app\models\Employees`.
 */
class EmployeesSearch extends Employees
{
    public $objectName;
    public $positionName;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'object_id', 'is_active', 'table_number'], 'integer'],
            [['full_name', 'objectName', 'positionName'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Employees::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->setSort([
            'attributes' => [
                'id' => [
                    'asc' => ['id' => SORT_DESC],
                    'desc' => ['id' => SORT_ASC]
                ],
                'table_number' => [
                    'asc' => ['table_number' => SORT_DESC],
                    'desc' => ['table_number' => SORT_ASC]
                ],
                'full_name' => [
                    'asc' => ['full_name' => SORT_ASC],
                    'desc' => ['full_name' => SORT_DESC]
                ],
                'objectName' => [
                    'asc' => ['objects.name' => SORT_ASC],
                    'desc' => ['objects.name' => SORT_DESC]
                ],
                'positionName' => [
                    'asc' => ['employee_positions.name' => SORT_ASC],
                    'desc' => ['employee_positions.name' => SORT_DESC]
                ],
                'is_active' => [
                    'asc' => ['is_active' => SORT_ASC],
                    'desc' => ['is_active' => SORT_DESC]
                ],

            ],
            'defaultOrder' => ['id' => SORT_DESC]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            $query->joinWith(['object']);
            $query->joinWith(['position']);
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'employees.id' => $this->id,
            'object_id' => $this->object_id,
            'position_id' => $this->position_id,
            'table_number' => $this->table_number,
            'is_active' => $this->is_active,
        ]);

        $query->andFilterWhere(['like', 'full_name', $this->full_name]);

        $query->joinWith(['object' => function ($q) {
            $condition = 'objects.name LIKE "%' . $this->objectName . '%"';
            if (empty($this->objectName)) {
                $condition .= ' OR objects.name IS NULL';
            }
            $q->where($condition);
        }]);

        $query->joinWith(['position' => function ($q) {
            $condition = 'employee_positions.name LIKE "%' . $this->positionName . '%"';
            if (empty($this->positionName)) {
                $condition .= ' OR employee_positions.name IS NULL';
            }
            $q->where($condition);
        }]);

        return $dataProvider;
    }
}
