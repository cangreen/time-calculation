<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

class TableForm extends Model
{
    public $month;
    public $object_id;
    public $record_type;

    public function rules()
    {
        return [
            [['month'], 'string'],
            [['object_id', 'record_type'], 'integer'],
            [['month', 'object_id', 'record_type'], 'required'],
        ];
    }

    public function getTable()
    {
        $records = $this->getRecords()->getModels();
        $date = explode('-', $this->month);
        $days_count = cal_days_in_month(CAL_GREGORIAN, $date[0], $date[1]);

        $table = [];
        $table_body = [];
        foreach ($records as $record) {
            $index = $record->employee_id . '-' . $record->position_id;
            $sum = 0;
            if (!array_key_exists($index, $table_body)) {
                $table_body[$index] = [
                    'full_name' => $record->employee->full_name,
                    'table_number' => $record->employee->table_number,
                    'position' => $record->positionName
                ];
                for ($i = 1; $i <= $days_count; $i++) {
                    $recordDate = Yii::$app->formatter->asTimestamp("{$i}.{$date[0]}.{$date[1]}");
                    if ($recordDate >= $record->getDay('start')['dayStart'] && $recordDate <= $record->getDay('end')['dayEnd']) {
                        if (!empty($record->table_code_id)) {
                            $table_body[$index][$i] = $record->code;
                        } else {
                            $table_body[$index][$i] = $record->end - $record->start;
                            $sum += $table_body[$index][$i];
                        }
                    } else {
                        $table_body[$index][$i] = 0;
                    }
                    $table_body[$index]['sum'] = $sum;
                }
            } else {
                for ($i = 1; $i <= $days_count; $i++) {
                    $recordDate = Yii::$app->formatter->asTimestamp("{$i}.{$date[0]}.{$date[1]}");
                    if ($recordDate >= $record->getDay('start')['dayStart'] && $recordDate <= $record->getDay('start')
                        ['dayEnd']) {
                        if (!empty($record->table_code_id)) {
                            $table_body[$index][$i] = $record->code;
                        } else {
                            $table_body[$index][$i] += $record->end - $record->start;
                            $table_body[$index]['sum'] += $record->end - $record->start;
                        }
                    } else {
                        continue;
                    }
                }
            }
        }

        $table_body_formatted = [];

        foreach ($table_body as $id => $record) {
            $table_body_formatted[$id]['ФИО'] = $table_body[$id]['full_name'];
            $table_body_formatted[$id]['Таб. №'] = $table_body[$id]['table_number'];
            $table_body_formatted[$id]['Должность'] = $table_body[$id]['position'];
            foreach ($record as $key => $value) {
                if (is_integer($key) && is_integer($value) || $key === 'sum') {
                    $table_body_formatted[$id][$key] = $this->formatInterval($value);
                } else if ($key !== 'full_name' && $key !== 'table_number' && $key !== 'position') {
                    $table_body_formatted[$id][$key] = $value;
                }
            }
            $sum = $table_body_formatted[$id]['sum'];
            unset($table_body_formatted[$id]['sum']);
            $table_body_formatted[$id]['Сумма'] = $sum;
        }
        $table['body'] = $table_body_formatted;
        return $table;
    }


    /**
     * @return ActiveDataProvider
     */
    private function getRecords()
    {
        $query = Records::find()->where([
            'object_id' => $this->object_id,
            'type' => $this->record_type
        ])->andWhere([
                'like', 'DATE_FORMAT(FROM_UNIXTIME(start), "%m-%Y")', $this->month]
        );

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => false,
            'pagination' => false,
        ]);

        return $dataProvider;
    }

    private function formatInterval($interval)
    {
        $hours = 0;
        $minutes = 0;
        if ($interval > 3600) {
            $hours = floor($interval / 3600);
            $interval = $interval % 3600;
        }
        $minutes += floor($interval / 60);
        $result = '';

        if ((int)$hours !== 0) {
            $result .= "{$hours}ч ";
        }
        if ((int)$minutes !== 0) {
            $result .= "{$minutes}м ";
        }
        return $result;
    }

    /**
     * @return string|null
     */
    public function getObjectName()
    {
        return Objects::findOne($this->object_id)->name;
    }


    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'objectName' => 'Объект',
            'object_id' => 'Объект',
            'record_type' => 'Тип записи'
        ];

    }
}