<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Records;
use yii\db\ActiveQuery;

/**
 * RecordsSearch represents the model behind the search form of `app\models\Records`.
 */
class RecordsSearch extends Records
{
    public $employeeFullName;
    public $positionName;
    public $employeeTableNumber;
    public $objectNumber;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'employee_id', 'object_id', 'type', 'employeeTableNumber', 'table_code_id'], 'integer'],
            [['objectNumber', 'employeeFullName', 'positionName', 'start', 'end', 'created_at', 'details'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Records::find();
        $user = Users::findOne(Yii::$app->user->identity->getId());
        if (Yii::$app->user->can('manager')) {
            $query->andWhere([
                'records.object_id' => $user->object_id
            ]);
        }
        if (Yii::$app->user->can('admin')) {
            $query->andWhere(['IN', 'records.object_id', Objects::find()->select('id')->where(['hub_id' => $user->hub_id])
            ]);
        }


        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->setSort([
            'attributes' => [
                'id' => [
                    'asc' => ['id' => SORT_DESC],
                    'desc' => ['id' => SORT_ASC]
                ],
                'employeeFullName' => [
                    'asc' => ['employees.full_name' => SORT_ASC],
                    'desc' => ['employees.full_name' => SORT_DESC]
                ],
                'employeeTableNumber' => [
                    'asc' => ['employees.table_number' => SORT_ASC],
                    'desc' => ['employees.table_number' => SORT_DESC]
                ],
                'positionName' => [
                    'asc' => ['employee_positions.name' => SORT_ASC],
                    'desc' => ['employee_positions.name' => SORT_DESC]
                ],
                'objectNumber' => [
                    'asc' => ['objects.object_number' => SORT_ASC],
                    'desc' => ['objects.object_number' => SORT_DESC]
                ],

                'type' => [
                    'asc' => ['type' => SORT_ASC],
                    'desc' => ['type' => SORT_DESC]
                ],
                'start' => [
                    'asc' => ['start' => SORT_ASC],
                    'desc' => ['start' => SORT_DESC]
                ],
                'end' => [
                    'asc' => ['end' => SORT_ASC],
                    'desc' => ['end' => SORT_DESC]
                ],
                'created_at' => [
                    'asc' => ['created_at' => SORT_ASC],
                    'desc' => ['created_at' => SORT_DESC]
                ],
                'details' => [
                    'asc' => ['details' => SORT_ASC],
                    'desc' => ['details' => SORT_DESC]
                ],
                'table_code_id' => [
                    'asc' => ['table_code_id' => SORT_ASC],
                    'desc' => ['table_code_id' => SORT_DESC]
                ]

            ],
            'defaultOrder' => ['id' => SORT_DESC]
        ]);


        $this->load($params);

        if (!empty($this->start)) {
            $this->filterByDate('start', $query);
        }

        if (!empty($this->end)) {
            $this->filterByDate('end', $query);
        }

        if (!empty($this->created_at)) {
            $this->filterByDate('created_at', $query);
        }

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            $query->joinWith(['employee']);
            $query->joinWith(['object']);
            $query->joinWith(['position']);
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'records.id' => $this->id,
            'employee_id' => $this->employee_id,
            'type' => $this->type,
            'object_id' => $this->object_id,
            'table_code_id' => $this->table_code_id,
//      'start' => $this->start,
//      'end' => $this->end,
//      'created_at' => $this->created_at,
        ]);

        $query->andFilterWhere(['like', 'records.details', $this->details]);

        $query->joinWith(['object' => function ($q) {
            $q->where('objects.object_number LIKE "%' . $this->objectNumber . '%"');
        }]);

        $query->joinWith(['employee' => function ($q) {
            $q->where('employees.full_name LIKE "%' . $this->employeeFullName . '%"');
        }]);
        $query->joinWith(['employee' => function ($q) {
            $q->where('employees.table_number LIKE "%' . $this->employeeTableNumber . '%"');
        }]);
        $query->joinWith(['position' => function (ActiveQuery $q) {
                $condition = 'employee_positions.name LIKE "%' . $this->positionName . '%"';
                if (empty($this->positionName)) {
                    $condition .= ' OR employee_positions.name IS NULL';
                }
                $q->where($condition);
        }]);


        return $dataProvider;
    }


    private function filterByDate($attr, $query)
    {
        $dayStart = (int)\Yii::$app->formatter->asTimestamp($this->$attr . ' 00:00:00');
        $dayStop = (int)\Yii::$app->formatter->asTimestamp($this->$attr . ' 23:59:59');
        $query->andFilterWhere([
            'between',
            self::tableName() . ".$attr",
            $dayStart,
            $dayStop,
        ]);
    }
}
