<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Users;

/**
 * UsersSearch represents the model behind the search form of `app\models\Users`.
 */
class UsersSearch extends Users
{
  public $objectName;

  /**
   * @inheritdoc
   */
  public function rules()
  {
    return [
      [['id', 'object_id', 'hub_id'], 'integer'],
      [['username', 'password_hash', 'email', 'objectName'], 'safe'],
    ];
  }

  /**
   * @inheritdoc
   */
  public function scenarios()
  {
    // bypass scenarios() implementation in the parent class
    return Model::scenarios();
  }

  /**
   * Creates data provider instance with search query applied
   *
   * @param array $params
   *
   * @return ActiveDataProvider
   */
  public function search($params)
  {
    $query = Users::find();

    // add conditions that should always apply here

    $dataProvider = new ActiveDataProvider([
      'query' => $query,
      'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
    ]);

    $dataProvider->setSort([
      'attributes' => [
        'id' => [
          'asc' => ['id' => SORT_DESC],
          'desc' => ['id' => SORT_ASC]
        ],
        'username' => [
          'asc' => ['username' => SORT_ASC],
          'desc' => ['username' => SORT_DESC]
        ],
        'email' => [
          'asc' => ['email' => SORT_ASC],
          'desc' => ['email' => SORT_DESC]
        ],
        'objectName' => [
          'asc' => ['objects.name' => SORT_ASC],
          'desc' => ['objects.name' => SORT_DESC]
        ],
          'hub_id' => [
          'asc' => ['hub_id' => SORT_ASC],
          'desc' => ['hub_id' => SORT_DESC]
        ],
      ],
      'defaultOrder' => ['id' => SORT_DESC]
    ]);

    $this->load($params);

    if (!$this->validate()) {
      // uncomment the following line if you do not want to return any records when validation fails
      // $query->where('0=1');
      $query->joinWith(['object']);
      return $dataProvider;
    }

    $this->addCondition($query, 'object_id');


    // grid filtering conditions
    $query->andFilterWhere([
      'users.id' => $this->id,
      'object_id' => $this->object_id,
      'hub_id' => $this->hub_id,
    ]);

    $query->andFilterWhere(['like', 'username', $this->username])
      ->andFilterWhere(['like', 'password_hash', $this->password_hash])
      ->andFilterWhere(['like', 'email', $this->email]);

    $query->joinWith(['object' => function ($q) {
      $q->where('objects.name LIKE "%' . $this->objectName . '%" OR objects.name IS NULL ');
    }]);

    return $dataProvider;
  }

  protected function addCondition($query, $attribute, $partialMatch = false)
  {
    if (($pos = strrpos($attribute, '.')) !== false) {
      $modelAttribute = substr($attribute, $pos + 1);
    } else {
      $modelAttribute = $attribute;
    }
    $value = $this->$modelAttribute;
    if (trim($value) === '') {
      return;
    }
    if ($partialMatch) {
      $query->andWhere(['like', $attribute, $value]);
    } else {
      $query->andWhere([$attribute => $value]);
    }
  }
}
