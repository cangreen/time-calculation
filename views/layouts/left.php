<aside class="main-sidebar">

    <section class="sidebar">
        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget' => 'tree'],
                'items' => [
                    ['label' => 'Меню', 'options' => ['class' => 'header']],
                    ['label' => 'Объекты', 'icon' => 'building', 'url' => ['/administrator/objects'], 'visible' =>
                        Yii::$app->user->can('superadmin')],
                    ['label' => 'ТУ', 'icon' => 'building', 'url' => ['/administrator/transport-hubs'], 'visible' =>
                        Yii::$app->user->can('superadmin')],
                    ['label' => 'Пользователи', 'icon' => 'user-o', 'url' => ['/administrator/users'], 'visible' =>
                        Yii::$app->user->can('superadmin')],
                    ['label' => 'Коды учёта', 'icon' => 'bold', 'url' => ['/administrator/table-codes'], 'visible' =>
                        Yii::$app->user->can('superadmin')],
                    ['label' => 'Сотрудники', 'icon' => 'users', 'url' => ['/administrator/employees'], 'visible' =>
                        Yii::$app->user->can('superadmin')],
                    ['label' => 'Должности', 'icon' => 'black-tie', 'url' => ['/administrator/employee-positions'], 'visible' =>
                        Yii::$app->user->can('superadmin')],
                    ['label' => 'Записи', 'icon' => 'clipboard', 'url' => ['/administrator/records'], 'visible' =>
                        Yii::$app->user->can('superadmin')],
                    ['label' => 'Записи', 'icon' => 'clipboard', 'url' => ['/administrator/records'], 'visible' =>
                        Yii::$app->user->can('admin')],
                    ['label' => 'Записи', 'icon' => 'clipboard', 'url' => ['/manager/records'], 'visible' =>
                        Yii::$app->user->can('manager')],
                    ['label' => 'Табель', 'icon' => 'table', 'url' => ['/administrator/table'], 'visible' =>
                        Yii::$app->user->can('superadmin')],
                    ['label' => 'Табель', 'icon' => 'table', 'url' => ['/administrator/table'], 'visible' =>
                        Yii::$app->user->can('admin')],
                    ['label' => 'Табель', 'icon' => 'table', 'url' => ['/manager/table'], 'visible' =>
                        Yii::$app->user->can('manager')],
                ],
            ]
        ) ?>

    </section>

</aside>
