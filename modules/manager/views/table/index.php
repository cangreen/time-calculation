<?php
/* @var $this \yii\web\View */

/* @var $model \app\models\TableForm */

use app\models\Users;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\DatePicker;
use kartik\grid\GridView;

$this->title = 'Табель';
$this->params['breadcrumbs'][] = $this->title;

$query = \app\models\Objects::find();
$user = Users::findOne(Yii::$app->user->identity->getId());
if(Yii::$app->user->can('admin')) {
  $query->andWhere(['hub_id' => $user->hub_id]);
}

?>

<div class="table-form-index box box-primary">
  <div class="box-header with-border">
    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'object_id')->dropDownList([$user->object_id => $user->object->name], [ 'disabled' => true]) ?>

    <?= $form->field($model, 'record_type')->dropDownList([
      1 => 'Рабочий',
      2 => 'Подработка',
      3 => 'Мобильный',
    ]) ?>
    
    <?= $form->field($model, 'month')->widget(DatePicker::classname(), [
      'options' => ['placeholder' => Yii::t('app', 'месяц/год')],
      'pluginOptions' => [
        'autoclose' => true,
        'startView' => 'year',
        'minViewMode' => 'months',
        'format' => 'mm-yyyy'
      ]
    ])->label(false); ?>

    <div class="box-footer">
      <?= Html::submitButton('Показать', ['class' => 'btn btn-success btn-flat']) ?>
    </div>

    <?php ActiveForm::end() ?>
  </div>
  <div class="box-body table-responsive no-padding">
    <?php if (!is_null($model->month)): ?>
      <?php $table = $model->getTable() ?>
      <?php
      $dataProvider = new \yii\data\ArrayDataProvider([
        'allModels' => $table['body']
      ]);
      ?>
      <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'responsive' => true,
        'hover' => true,
        'toolbar' => [
          [
            'options' => ['class' => 'btn-group mr-2']
          ],
          '{export}',
        ],
        'export' => [
          'label' => 'Выгрузить',
          'showConfirmAlert' => false,
          'header' => '',
        ],
        'exportConfig' => [
          GridView::EXCEL => ['label' => 'Excel'],

        ],
        'panel' => [
          'type' => GridView::TYPE_DEFAULT,
        ],


      ]) ?>
    <?php endif; ?>
  </div>
</div>




