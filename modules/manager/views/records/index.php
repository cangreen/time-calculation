<?php

use app\models\TableCodes;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use kartik\grid\GridView;
use app\models\Records;
use kartik\widgets\DatePicker;

/* @var $this yii\web\View */
/* @var $searchModel app\models\RecordsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Записи';
$this->params['breadcrumbs'][] = $this->title;
$types = [
    1 => 'Рабочий',
    2 => 'Подработка',
    3 => 'Мобильный',
];
$codes = ArrayHelper::map(TableCodes::find()->all(), 'id', 'code');

?>
<div class="records-index box box-primary">
    <div class="box-header with-border">
        <?= Html::a('Добавить запись', ['create'], ['class' => 'btn btn-success btn-flat']) ?>
    </div>
    <div class="box-body table-responsive no-padding">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'layout' => "{items}\n{summary}\n{pager}",
            'responsive' => true,
            'hover' => true,
            'krajeeDialogSettings' => ['useNative' => true],
            'toolbar' => [
                [
                    'options' => ['class' => 'btn-group mr-2']
                ],
                '{export}',
                '{toggleData}',
            ],
            'export' => [
                'label' => 'Выгрузить',
                'showConfirmAlert' => false,
                'header' => '',
            ],
            'exportConfig' => [
                GridView::EXCEL => ['label' => 'Excel'],

            ],
            'panel' => [
                'type' => GridView::TYPE_DEFAULT,
            ],
            'columns' => [
                [
                    'attribute' => 'created_at',
                    'value' => function (Records $model) {
                        return Yii::$app->formatter->asDatetime($model->created_at);
                    },
                    'filter' => DatePicker::widget([
                        'model' => $searchModel,
                        'attribute' => 'created_at',
                        'language' => 'ru',
                        'pluginOptions' => [
                            'autoclose' => true,
                            'todayHighlight' => true,
                            'format' => 'dd.mm.yyyy',
                        ],
                    ]),
                ],
                'employeeFullName',
                'employeeTableNumber',
                'positionName',
                'objectNumber',
                [
                    'attribute' => 'type',
                    'filter' => $types,
                    'value' => function (Records $model) {
                        $types = [
                            1 => 'Рабочий',
                            2 => 'Подработка',
                            3 => 'Мобильный',
                        ];
                        return $types[$model->type];
                    }
                ],
                [
                    'attribute' => 'start',
                    'value' => function (Records $model) {
                        return Yii::$app->formatter->asDatetime($model->start);
                    },
                    'filter' => DatePicker::widget([
                        'model' => $searchModel,
                        'attribute' => 'start',
                        'language' => 'ru',
                        'pluginOptions' => [
                            'autoclose' => true,
                            'todayHighlight' => true,
                            'format' => 'dd.mm.yyyy',
                        ],
                    ]),
                ],
                [
                    'attribute' => 'end',
                    'value' => function (Records $model) {
                        return Yii::$app->formatter->asDatetime($model->end);
                    },
                    'filter' => DatePicker::widget([
                        'model' => $searchModel,
                        'attribute' => 'end',
                        'language' => 'ru',
                        'pluginOptions' => [
                            'autoclose' => true,
                            'todayHighlight' => true,
                            'format' => 'dd.mm.yyyy',
                        ],
                    ]),
                ],
                [
                    'attribute' => 'table_code_id',
                    'filter' => $codes,
                    'value' => function (Records $model) {
                        return $model->code;
                    }
                ],
                [
                    'label' => 'Часы',
                    'value' => function (Records $model) {

                        return !empty($model->table_code_id) ? '' : $model->formatSecondsInterval();
                    }

                ],
                'details',
                // 'updated_at',

                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{view} {update}'
                ],
            ],
        ]); ?>
    </div>
</div>
