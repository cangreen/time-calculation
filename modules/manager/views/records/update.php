<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Records */
$created_at = Yii::$app->formatter->asDatetime($model->created_at);
$this->title = 'Редактирование записи: ' . $created_at;
$this->params['breadcrumbs'][] = ['label' => 'Записи', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $created_at, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'редактирование';
?>
<div class="records-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
