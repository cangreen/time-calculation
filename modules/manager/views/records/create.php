<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Records */

$this->title = 'Добавление записи';
$this->params['breadcrumbs'][] = ['label' => 'Записи', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="records-create">

    <?= $this->render('_form', [
    'model' => $model,
    ]) ?>

</div>
