<?php

namespace app\modules\manager\controllers;

use app\models\TableForm;
use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;

/**
 * TableController Вывод табеля.
 */
class TableController extends Controller
{
  /**
   * @inheritdoc
   */
  public function behaviors()
  {
    return [
      'verbs' => [
        'class' => VerbFilter::className(),
        'actions' => [
          'delete' => ['POST'],
        ],
      ],
      'access' => [
        'class' => '\yii\filters\AccessControl',
        'rules' => [
          [
            'allow' => true,
            'roles' => ['admin', 'manager']
          ],
        ],
      ],
    ];
  }


  public function actionIndex()
  {
    $model = new TableForm();
    $post = Yii::$app->request->post();
    $user = \app\models\Users::findOne(Yii::$app->user->identity->getId());
    $model->object_id = $user->object_id;
    if (!empty($post)) {
      $model->load(Yii::$app->request->post());
    }
    return $this->render('index', [
      'model' => $model,
    ]);
  }
}
