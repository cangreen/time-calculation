<?php

namespace app\modules\manager\controllers;

use app\models\TableCodes;
use Yii;
use app\models\Employees;
use app\models\EmployeesSearch;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * EmployeesController implements the CRUD actions for Employees model.
 */
class EmployeesController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => '\yii\filters\AccessControl',
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['manager']
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Employees models.
     * @return mixed
     */


    public function actionGetEmployees()
    {
        $user = \app\models\Users::findOne(Yii::$app->user->identity->getId());
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
                $type_id = $parents[0];
                $object_id = (int)$type_id !== 3 ? $user->object_id : null;
                $query = Employees::find()->where(['is_active' => true]);
                if ((int)$type_id === 1) {
                    $query->andWhere(['object_id' => $object_id]);
                }

                if ((int)$type_id === 3) {
                    $query->where(['object_id' => null]);
                }
                foreach (ArrayHelper::toArray($query->all()) as $employee) {
                    $out[] = ['id' => $employee['id'], 'name' => $employee['full_name']];
                }


                return ['output' => $out, 'selected' => ''];
            }
        }
        return ['output' => '', 'selected' => ''];
    }

    public function actionGetCodes()
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = [];
        $parents = $_POST['depdrop_parents'];
        $type_id = $parents[0];

        if ((int)$type_id === 1) {
            $query = TableCodes::find()->where(['admin_only' => false]);
            $codes = ArrayHelper::map($query->all(), 'id', 'code');

            foreach ($codes as $key => $code) {
                $out[] = ['id' => $key, 'name' => $code];
            }
        }
        return ['output' => $out, 'selected' => ''];
    }


}
