<?php

namespace app\modules\administrator\controllers;

use app\models\TableForm;
use Yii;
use app\models\Records;
use app\models\RecordsSearch;
use yii\web\Controller;
use yii\filters\VerbFilter;

/**
 * TableController Вывод табеля.
 */
class TableController extends Controller
{
  /**
   * @inheritdoc
   */
  public function behaviors()
  {
    return [
      'verbs' => [
        'class' => VerbFilter::className(),
        'actions' => [
          'delete' => ['POST'],
        ],
      ],
      'access' => [
        'class' => '\yii\filters\AccessControl',
        'rules' => [
          [
            'allow' => true,
            'roles' => ['admin', 'superadmin']
          ],
        ],
      ],
    ];
  }


  public function actionIndex()
  {
    $model = new TableForm();
    $post = Yii::$app->request->post();
    if (!empty($post)) {
      $model->load(Yii::$app->request->post());
    }
    return $this->render('index', [
      'model' => $model,
    ]);
  }
}
