<?php

namespace app\modules\administrator\controllers;

use app\models\TableCodes;
use Yii;
use app\models\Employees;
use app\models\EmployeesSearch;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * EmployeesController implements the CRUD actions for Employees model.
 */
class EmployeesController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => '\yii\filters\AccessControl',
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['superadmin', 'admin']
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Employees models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new EmployeesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        if (!Yii::$app->user->can('superadmin')) {
            throw new ForbiddenHttpException('Доступ запрещен');
        }
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Employees model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        if (!Yii::$app->user->can('superadmin')) {
            throw new ForbiddenHttpException('Доступ запрещен');
        }

        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Employees model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if (!Yii::$app->user->can('superadmin')) {
            throw new ForbiddenHttpException('Доступ запрещен');
        }
        $model = new Employees();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Employees model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        if (!Yii::$app->user->can('superadmin')) {
            throw new ForbiddenHttpException('Доступ запрещен');
        }
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Employees model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        if (!Yii::$app->user->can('superadmin')) {
            throw new ForbiddenHttpException('Доступ запрещен');
        }

        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Employees model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Employees the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Employees::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Список позиций для виджета DepDrop во вью _form
     * @return array
     */
    public function actionGetEmployees()
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = [];
        $parents = $_POST['depdrop_parents'];
        $object_id = $parents[0];
        $type_id = $parents[1];

        if (isset($parents) && !empty($object_id) && !empty($type_id)) {
            $query = Employees::find()->where(['is_active' => true]);
            if((int)$type_id === 1) {
                $query->andWhere(['object_id' => $object_id]);
            }

            if((int)$type_id === 3) {
                $query->where(['object_id' => null]);
            }

            foreach (ArrayHelper::toArray($query->all()) as $employee) {
                $out[] = ['id' => $employee['id'], 'name' => $employee['full_name']];
            }
            return ['output' => $out, 'selected' => ''];
        }
        return ['output' => '', 'selected' => ''];
    }

    public function actionGetCodes()
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = [];
        $parents = $_POST['depdrop_parents'];
        $type_id = $parents[0];

        if ((int)$type_id === 1) {
            $codes = ArrayHelper::map(TableCodes::find()->all(), 'id', 'code');

            foreach ($codes as $key => $code) {
                $out[] = ['id' => $key, 'name' => $code];
            }
        }
        return ['output' => $out, 'selected' => ''];
    }


}
