<?php

namespace app\modules\administrator\controllers;

use app\models\EmployeePositions;
use app\models\Employees;
use Yii;
use app\models\Records;
use app\models\RecordsSearch;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * RecordsController implements the CRUD actions for Records model.
 */
class RecordsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => '\yii\filters\AccessControl',
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['admin', 'superadmin']
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Records models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new RecordsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Records model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Records model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Records();
        if (!empty($_POST['Records']['start_period']) && !empty($_POST['Records']['end_period'])) {
            $model->scenario = Records::SCENARIO_TABLE_CODE;
            if ($model->load(Yii::$app->request->post()) && $this->createMultipleRecords($model)) {
                return $this->redirect(['index']);
            }
        } else if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Records model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Records model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Records model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Records the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Records::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function createMultipleRecords($model)
    {
        $daysOfPeriod = $model->getDaysOfPeriod($model->start_period, $model->end_period);
        foreach ($daysOfPeriod as $day) {
            $record = new Records();
            $record->created_at = $model->created_at;
            $record->object_id = $model->object_id;
            $record->employee_id = $model->employee_id;
            $record->type = $model->type;
            $record->start = $day;
            $record->end = $day + 86399;
            $record->table_code_id = $model->table_code_id;
            $record->details = $model->details;
            $record->save();
        }
        return true;
    }

    /**
     * Получение данных для виджета DepDrop во вью _form
     * @return array
     */
    public function actionGetPositions()
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = [];
        $parents = $_POST['depdrop_parents'];
        $employee_id = $parents[0];
        $type_id = $parents[1];

        if (isset($parents) && !empty($employee_id) && !empty($type_id)) {

            $positions = null;

            if ((int)$type_id === 1 || (int)$type_id === 3) {
                $employee = Employees::findOne($employee_id);
                $positions = EmployeePositions::find()->where(['id' => $employee->position_id])->all();
            }

            if ((int)$type_id === 2) {
                $positions = EmployeePositions::find()->all();
            }

            foreach (ArrayHelper::toArray($positions) as $position) {
                $out[] = ['id' => $position['id'], 'name' => $position['name']];
            }
            return ['output' => $out, 'selected' => ''];
        }
        return ['output' => '', 'selected' => ''];
    }
}
