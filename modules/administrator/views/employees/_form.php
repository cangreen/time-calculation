<?php

use app\models\EmployeePositions;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Objects;
use kartik\widgets\Select2;


/* @var $this yii\web\View */
/* @var $model app\models\Employees */
/* @var $form yii\widgets\ActiveForm */

$objects = ArrayHelper::map(Objects::find()->all(), 'id', 'name');

$positions = ArrayHelper::map(EmployeePositions::find()->all(), 'id', 'name');

?>

<div class="employees-form box box-primary">
    <?php $form = ActiveForm::begin(); ?>
    <div class="box-body table-responsive">

        <?= $form->field($model, 'object_id')->widget(Select2::class, [
            'data' => $objects,
            'options' => ['placeholder' => 'Выберите объект ...'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]) ?>
        <?= $form->field($model, 'position_id')->widget(Select2::class, [
            'data' => $positions,
            'options' => ['placeholder' => 'Выберите должность ...'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]) ?>

        <?= $form->field($model, 'table_number')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'full_name')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'is_active')->dropDownList([
            1 => 'Да',
            0 => 'Нет'
        ]) ?>

    </div>
    <div class="box-footer">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success btn-flat']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
