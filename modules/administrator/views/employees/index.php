<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\EmployeesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Сотрудники';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="employees-index box box-primary">
  <div class="box-header with-border">
    <?= Html::a('Добавить сотрудника', ['create'], ['class' => 'btn btn-success btn-flat']) ?>
  </div>
  <div class="box-body table-responsive no-padding">
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <?= GridView::widget([
      'dataProvider' => $dataProvider,
      'filterModel' => $searchModel,
      'formatter' => ['class' => 'yii\i18n\Formatter'],
      'layout' => "{items}\n{summary}\n{pager}",
      'columns' => [
        'table_number',
        'full_name',
        'positionName',
        'objectName',
        [
          'attribute' => 'is_active',
          'filter' => [
            1 => 'Да',
            0 => 'Нет'
          ],
          'value' => function (\app\models\Employees $model) {
            return $model->is_active ? 'Да' : 'Нет';
          }
        ],


        ['class' => 'yii\grid\ActionColumn'],
      ],
    ]); ?>
  </div>
</div>
