<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Employees */

$this->title = $model->full_name;
$this->params['breadcrumbs'][] = ['label' => 'Сотрудники', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>


<div class="employees-view box box-primary">
  <div class="box-header">
    <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary btn-flat']) ?>
    <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
      'class' => 'btn btn-danger btn-flat',
      'data' => [
        'confirm' => 'Удалить сотрудника?',
        'method' => 'post',
      ],
    ]) ?>
  </div>
  <div class="box-body table-responsive no-padding">
    <?= DetailView::widget([
      'model' => $model,
      'attributes' => [
        'table_number',
        'objectName',
        'positionName',
        'full_name',
        [
          'attribute' => 'is_active',
          'value' => function (\app\models\Employees $model) {
            return $model->is_active ? 'Да' : 'Нет';
          }
        ],
      ],
    ]) ?>
  </div>
</div>
