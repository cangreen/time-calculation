<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\TableCodes */

$this->title = 'Создание кода учёта';
$this->params['breadcrumbs'][] = ['label' => 'Коды учёта', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="table-codes-create">

    <?= $this->render('_form', [
    'model' => $model,
    ]) ?>

</div>
