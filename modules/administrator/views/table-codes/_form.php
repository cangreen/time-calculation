<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TableCodes */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="table-codes-form box box-primary">
    <?php $form = ActiveForm::begin(); ?>
    <div class="box-body table-responsive">

        <?= $form->field($model, 'code')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'details')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'is_periodic')->dropDownList([0 => 'Нет', 1 => 'Да' ]) ?>

        <?= $form->field($model, 'admin_only')->dropDownList([0 => 'Нет', 1 => 'Да' ]) ?>

    </div>
    <div class="box-footer">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success
      btn-flat']) ?>
      <?= Html::a('Отменить', '/administrator/table-codes/index',
      ['class' => 'btn
      btn-warning
      btn-flat']) ?>

    </div>
    <?php ActiveForm::end(); ?>
</div>
