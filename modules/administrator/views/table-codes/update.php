<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TableCodes */

$this->title = 'Редактирование кода учёта: ' . $model->code;
$this->params['breadcrumbs'][] = ['label' => 'Table Codes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'редактирование';
?>
<div class="table-codes-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
