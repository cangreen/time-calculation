<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Records */

$this->title = "Запись: {$model->employee->full_name} в {$model->object->name} " . Yii::$app->formatter->asDatetime($model->created_at);
$this->params['breadcrumbs'][] = ['label' => 'Записи', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="records-view box box-primary">
  <div class="box-header">
    <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary btn-flat']) ?>
    <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
      'class' => 'btn btn-danger btn-flat',
      'data' => [
        'confirm' => 'Удалить запись?',
        'method' => 'post',
      ],
    ]) ?>
  </div>
  <div class="box-body table-responsive no-padding">
    <?= DetailView::widget([
      'model' => $model,
      'attributes' => [
        'id',
        'employeeFullName',
        'objectName',
        'start:datetime',
        'end:datetime',
        'created_at:datetime',
        'details'
      ],
    ]) ?>
  </div>
</div>
