<?php

use app\models\EmployeePositions;
use app\models\Users;
use kartik\widgets\Select2;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\depdrop\DepDrop;
use yii\helpers\ArrayHelper;
use app\models\Objects;
use app\models\TableCodes;
use yii\helpers\Url;
use kartik\datecontrol\DateControl;

\app\assets\AdminAsset::register($this);

/* @var $this yii\web\View */
/* @var $model app\models\Records */
/* @var $form yii\widgets\ActiveForm */

$query = Objects::find();
$codesQuery = TableCodes::find();
$user = Users::findOne(Yii::$app->user->identity->getId());

if (Yii::$app->user->can('admin')) {
    $query->andWhere(['hub_id' => $user->hub_id]);
}

$objects = ArrayHelper::map($query->all(), 'id', 'name');
$codes = ArrayHelper::map($codesQuery->all(), 'id', 'code');
$positions = ArrayHelper::map(EmployeePositions::find()->all(), 'id', 'name');


?>

    <div class="records-form box box-primary">
        <?php $form = ActiveForm::begin(['enableClientValidation' => false]); ?>
        <div class="box-body table-responsive">

            <?= $form->field($model, 'created_at')->widget(DateControl::classname(), [
                'type' => DateControl::FORMAT_DATETIME,
                'widgetOptions' => [
                    'pluginOptions' => [
                        'autoclose' => true,
                        'todayBtn' => true
                    ]
                ]
            ]); ?>

            <?= $form->field($model, 'object_id')->widget(Select2::className(), [
                'data' => $objects,
                'options' => ['placeholder' => 'Выберите объект ...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]) ?>

            <?= $form->field($model, 'type')->dropDownList([
                null => '...',
                1 => 'Рабочий',
                2 => 'Подработка',
                3 => 'Мобильный',
            ]) ?>

            <?= $form->field($model, 'employee_id')->widget(DepDrop::className(), [
                'type' => DepDrop::TYPE_SELECT2,
                'select2Options' => ['pluginOptions' => ['allowClear' => true]],
                'options' => [
                    'placeholder' => !empty($model->employee) ? $model->employee->full_name : ''
                ],
                'pluginOptions' => [
                    'depends' => ['records-object_id', 'records-type'],
                    'initialize' => true,
                    'placeholder' => 'Выбор сотрудника...',
                    'url' => Url::to(['/administrator/employees/get-employees'])
                ]
            ]) ?>
            <div>
                <?= $form->field($model, 'position_id')->widget(DepDrop::className(), [
                    'type' => DepDrop::TYPE_SELECT2,
                    'select2Options' => ['pluginOptions' => ['allowClear' => true]],
                    'pluginOptions' => [
                        'depends' => ['records-employee_id', 'records-type'],
                        'initialize' => true,
                        'placeholder' => 'Выбор должности...',
                        'url' => Url::to(['/administrator/records/get-positions'])
                    ]
                ]) ?>
            </div>
            <div style="display: none">
                <?= $form->field($model, 'position_id')->dropDownList([
                    $model->id => $model->position->name
                ], [
                    'id' => 'records-position_id_selected',
                    'name' => ''
                ]) ?>
            </div>
            <?= $form->field($model, 'table_code_id')->widget(DepDrop::className(), [
                'pluginOptions' => [
                    'depends' => ['records-type'],
                    'initialize' => true,
                    'placeholder' => 'Выбор кода учёта...',
                    'url' => Url::to(['/administrator/employees/get-codes'])
                ]
            ]) ?>

            <div class="record-day">
                <?= $form->field($model, 'start')->widget(DateControl::classname(), [
                    'type' => DateControl::FORMAT_DATETIME,
                    'widgetOptions' => [
                        'pluginOptions' => [
                            'autoclose' => true,
                            'todayBtn' => true
                        ]
                    ]
                ]); ?>


                <?= $form->field($model, 'end')->widget(DateControl::classname(), [
                    'type' => DateControl::FORMAT_DATETIME,
                    'widgetOptions' => [
                        'pluginOptions' => [
                            'autoclose' => true,
                            'todayBtn' => true
                        ]
                    ]
                ]); ?>
            </div>

            <div class="record-period" style="display: none;">
                <?= $form->field($model, 'start_period')->widget(DateControl::classname(), [
                    'type' => DateControl::FORMAT_DATE,
                    'widgetOptions' => [
                        'pluginOptions' => [
                            'autoclose' => true,
                            'todayBtn' => true
                        ]
                    ]
                ]); ?>


                <?= $form->field($model, 'end_period')->widget(DateControl::classname(), [
                    'type' => DateControl::FORMAT_DATE,
                    'widgetOptions' => [
                        'pluginOptions' => [
                            'autoclose' => true,
                            'todayBtn' => true
                        ]
                    ]
                ]); ?>

            </div>
            <?= $form->field($model, 'details')->textarea() ?>
        </div>
        <div class="box-footer">
            <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success btn-flat']) ?>
        </div>
        <?php ActiveForm::end(); ?>
    </div>

<?php
$this->registerJsFile('/js/recordsForm.js', ['depends' => \yii\web\JqueryAsset::className()]);