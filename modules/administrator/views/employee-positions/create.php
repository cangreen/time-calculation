<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\EmployeePositions */

$this->title = 'Создание должности';
$this->params['breadcrumbs'][] = ['label' => 'Должности', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="employee-positions-create">

    <?= $this->render('_form', [
    'model' => $model,
    ]) ?>

</div>
