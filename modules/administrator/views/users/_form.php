<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Objects;

/* @var $this yii\web\View */
/* @var $model app\models\Users */
/* @var $form yii\widgets\ActiveForm */


$objects = ArrayHelper::map(Objects::find()->all(), 'id', 'name');
$hubs[null] = '...';
$hubs = $hubs + ArrayHelper::map(\app\models\TransportHubs::find()->all(), 'id', 'name');
?>

<div class="users-form box box-primary">
  <?php $form = ActiveForm::begin(); ?>
  <div class="box-body table-responsive">

    <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?php if (Yii::$app->authManager->checkAccess($model->id, 'manager')): ?>

      <?= $form->field($model, 'object_id')->dropDownList($objects) ?>

    <?php elseif ($model->id !== Yii::$app->user->identity->getId()): ?>

      <?= $form->field($model, 'hub_id')->dropDownList($hubs) ?>

    <?php endif; ?>

  </div>
  <div class="box-footer">
    <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success btn-flat']) ?>
  </div>
  <?php ActiveForm::end(); ?>
</div>



