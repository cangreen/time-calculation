<?php

use app\models\Objects;
use app\models\TransportHubs;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\ActiveForm;
use kartik\widgets\DepDrop;


/* @var $this yii\web\View */
/* @var $model app\models\Users */

$this->title = 'Создание пользователя';
$this->params['breadcrumbs'][] = ['label' => 'Пользователи', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

\app\assets\AdminAsset::register($this);


$hubs[null] = '...';;
$hubs = $hubs + ArrayHelper::map(TransportHubs::find()->all(), 'id', 'name');
$objects = ArrayHelper::map(Objects::find()->all(), 'id', 'name');
?>
<div class="users-create">

  <div class="users-form box box-primary">
      <?php $form = ActiveForm::begin(); ?>
    <div class="box-body table-responsive">

        <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'password')->passwordInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'isAdmin')->checkbox(['id' => 'users-isadmin']) ?>

            <?= $form->field($model, 'hub_id')->widget(\kartik\depdrop\DepDrop::className(), [
            'options' => [
                'placeholder' => !empty($model->hub_id) ? $model->hub->name : ''
            ],
            'pluginOptions' => [
                'depends' => ['users-isadmin'],
                'placeholder' => 'Выбор ТУ...',
                'url' => Url::to(['/administrator/users/get-hubs'])
            ]
        ])?>

            <?= $form->field($model, 'object_id')->dropDownList($objects) ?>

    </div>
    <div class="box-footer">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success btn-flat']) ?>
    </div>
      <?php ActiveForm::end(); ?>
  </div>

</div>

