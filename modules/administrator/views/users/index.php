<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UsersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Пользователи';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="users-index box box-primary">
  <div class="box-header with-border">
      <?= Html::a('Добавить пользователя', ['create'], ['class' => 'btn btn-success btn-flat']) ?>
  </div>
  <div class="box-body table-responsive no-padding">
      <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
      <?= GridView::widget([
          'dataProvider' => $dataProvider,
          'filterModel' => $searchModel,
          'layout' => "{items}\n{summary}\n{pager}",
          'columns' => [

              'id',
              'username',
//                'password_hash',
              'email:email',
              'objectName',
              'hub_id',
              [
                  'label' => 'Роль',
                  'value' => function (\app\models\Users $model) {
                      $am = Yii::$app->authManager;
                      if ($am->checkAccess($model->id, 'admin')) {
                          return 'Админ';
                      } else if ($am->checkAccess($model->id, 'superadmin')) {
                          return 'Суперадмин';
                      } else {
                          return 'Менеджер';
                      }
                  },

              ],
              [
                  'class' => 'yii\grid\ActionColumn',
                  'template' => '{view} {update} {delete} {change-password}',
                  'buttons' => [
                      'change-password' => function ($url) {
                          return Html::a(
                              '<span class="glyphicon glyphicon-lock" aria-hidden="true"></span>',
                              $url,
                              ['title' => 'Сменить пароль']
                          );
                      }
                  ]
              ],
          ],
      ]); ?>
  </div>
</div>
