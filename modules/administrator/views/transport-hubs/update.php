<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TransportHubs */

$this->title = 'Редактирование ТУ: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Транспортные узлы', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'редактирование';
Html::a('dsf')
?>
<div class="transport-hubs-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
