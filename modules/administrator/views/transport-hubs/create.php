<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\TransportHubs */

$this->title = 'Создание ТУ';
$this->params['breadcrumbs'][] = ['label' => 'Транспортные узлы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="transport-hubs-create">

    <?= $this->render('_form', [
    'model' => $model,
    ]) ?>

</div>
