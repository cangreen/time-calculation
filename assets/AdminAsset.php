<?php


namespace app\assets;


class AdminAsset extends AppAsset
{
    public $js = [
        'js/admin.js'
    ];

}