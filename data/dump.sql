-- MySQL dump 10.13  Distrib 5.7.28, for Linux (x86_64)
--
-- Host: 127.0.0.1    Database: tabel
-- ------------------------------------------------------
-- Server version	5.7.28-0ubuntu0.18.04.4

/*!40101 SET @OLD_CHARACTER_SET_CLIENT = @@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS = @@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION = @@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE = @@TIME_ZONE */;
/*!40103 SET TIME_ZONE = '+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS = @@UNIQUE_CHECKS, UNIQUE_CHECKS = 0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS = @@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS = 0 */;
/*!40101 SET @OLD_SQL_MODE = @@SQL_MODE, SQL_MODE = 'NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES = @@SQL_NOTES, SQL_NOTES = 0 */;

--
-- Table structure for table `auth_assignment`
--

DROP TABLE IF EXISTS `auth_assignment`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_assignment`
(
    `item_name`  varchar(64) COLLATE utf8_unicode_ci NOT NULL,
    `user_id`    varchar(64) COLLATE utf8_unicode_ci NOT NULL,
    `created_at` int(11) DEFAULT NULL,
    PRIMARY KEY (`item_name`, `user_id`),
    KEY `idx-auth_assignment-user_id` (`user_id`),
    CONSTRAINT `auth_assignment_ibfk_1` FOREIGN KEY (`item_name`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COLLATE = utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_assignment`
--

LOCK TABLES `auth_assignment` WRITE;
/*!40000 ALTER TABLE `auth_assignment`
    DISABLE KEYS */;
INSERT INTO `auth_assignment`
VALUES ('admin', '1', NULL),
       ('admin', '13', 1579127288),
       ('admin', '15', 1579128818),
       ('admin', '16', 1579129257),
       ('manager', '14', 1579127323),
       ('manager', '17', 1579129295);
/*!40000 ALTER TABLE `auth_assignment`
    ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_item`
--

DROP TABLE IF EXISTS `auth_item`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_item`
(
    `name`        varchar(64) COLLATE utf8_unicode_ci NOT NULL,
    `type`        smallint(6)                         NOT NULL,
    `description` text COLLATE utf8_unicode_ci,
    `rule_name`   varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
    `data`        blob,
    `created_at`  int(11)                             DEFAULT NULL,
    `updated_at`  int(11)                             DEFAULT NULL,
    PRIMARY KEY (`name`),
    KEY `rule_name` (`rule_name`),
    KEY `idx-auth_item-type` (`type`),
    CONSTRAINT `auth_item_ibfk_1` FOREIGN KEY (`rule_name`) REFERENCES `auth_rule` (`name`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COLLATE = utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_item`
--

LOCK TABLES `auth_item` WRITE;
/*!40000 ALTER TABLE `auth_item`
    DISABLE KEYS */;
INSERT INTO `auth_item`
VALUES ('admin', 1, NULL, NULL, NULL, NULL, NULL),
       ('manager', 1, NULL, NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `auth_item`
    ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_item_child`
--

DROP TABLE IF EXISTS `auth_item_child`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_item_child`
(
    `parent` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
    `child`  varchar(64) COLLATE utf8_unicode_ci NOT NULL,
    PRIMARY KEY (`parent`, `child`),
    KEY `child` (`child`),
    CONSTRAINT `auth_item_child_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT `auth_item_child_ibfk_2` FOREIGN KEY (`child`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COLLATE = utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_item_child`
--

LOCK TABLES `auth_item_child` WRITE;
/*!40000 ALTER TABLE `auth_item_child`
    DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_item_child`
    ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_rule`
--

DROP TABLE IF EXISTS `auth_rule`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_rule`
(
    `name`       varchar(64) COLLATE utf8_unicode_ci NOT NULL,
    `data`       blob,
    `created_at` int(11) DEFAULT NULL,
    `updated_at` int(11) DEFAULT NULL,
    PRIMARY KEY (`name`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COLLATE = utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_rule`
--

LOCK TABLES `auth_rule` WRITE;
/*!40000 ALTER TABLE `auth_rule`
    DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_rule`
    ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `employees`
--

DROP TABLE IF EXISTS `employees`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `employees`
(
    `id`        int(11) NOT NULL AUTO_INCREMENT,
    `object_id` int(11)                              DEFAULT NULL,
    `full_name` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
    `is_active` tinyint(1)                           DEFAULT NULL,
    PRIMARY KEY (`id`),
    KEY `employee_object_fk` (`object_id`),
    CONSTRAINT `employee_object_fk` FOREIGN KEY (`object_id`) REFERENCES `objects` (`id`) ON DELETE SET NULL
) ENGINE = InnoDB
  AUTO_INCREMENT = 51
  DEFAULT CHARSET = utf8
  COLLATE = utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `employees`
--

LOCK TABLES `employees` WRITE;
/*!40000 ALTER TABLE `employees`
    DISABLE KEYS */;
INSERT INTO `employees`
VALUES
       (2, 8, 'Malcolm Effertz', 1),
       (3, 2, 'Meda Schultz', 0),
       (4, 1, 'Roselyn Barrows', 1),
       (5, 9, 'Prof. Jarret Vandervort DDS', 0),
       (6, 8, 'D\'angelo Beahan I', 0),
       (8, 5, 'Melyna Rempel', 0),
       (9, 7, 'Darien Roob IV', 0),
       (10, 3, 'Erin Wilkinson', 0),
       (12, 4, 'Jeanne Kilback', 1),
       (13, 8, 'Amos Spencer', 0),
       (14, 4, 'Prof. Jan Hand', 0),
       (15, 9, 'Prof. Arvilla Watsica', 0),
       (17, 9, 'Dillon O\'Reilly', 0),
       (18, 8, 'Reanna Zulauf', 1),
       (19, 2, 'Chris Turner', 1),
       (20, 8, 'Pietro Rath', 0),
       (21, 4, 'Deontae Mayer III', 0),
       (22, 5, 'Ms. Patience Ward', 0),
       (24, 1, 'Joelle Berge Jr.', 0),
       (25, 1, 'Wilburn Batz', 1),
       (26, 8, 'Darrion Stokes', 0),
       (27, 7, 'Miss Thelma Leffler I', 1),
       (28, 2, 'Phoebe Feest I', 0),
       (29, 7, 'Ima Jaskolski', 1),
       (30, 7, 'Dr. Travis Russel', 1),
       (31, 7, 'Skylar Pacocha PhD', 1),
       (32, 5, 'Naomie Wunsch III', 0),
       (33, 6, 'Elian Kerluke', 0),
       (34, 8, 'Kory Jerde', 1),
       (35, 4, 'Susan Marvin', 0),
       (36, 5, 'Kailyn Rutherford', 1),
       (37, 9, 'Miss Aniyah O\'Kon', 0),
       (38, 1, 'Jarod Spinka', 0),
       (39, 3, 'Wendell Berge', 0),
       (40, 7, 'Marge Dickens', 1),
       (41, 8, 'Flo Wehner', 0),
       (42, 2, 'Aditya Kassulke', 1),
       (43, 4, 'Destiney Langworth', 1),
       (44, 8, 'Mr. Kristopher Walter', 0),
       (45, 9, 'Nella Harvey III', 1),
       (47, 1, 'Prof. Clifton Mohr', 1),
       (48, 1, 'Dr. Danny Jast Jr.', 1),
       (49, 8, 'Gay Adams', 0),
       (50, 6, 'Mikel Kulas', 0);
/*!40000 ALTER TABLE `employees`
    ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migration`
--

DROP TABLE IF EXISTS `migration`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migration`
(
    `version`    varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
    `apply_time` int(11) DEFAULT NULL,
    PRIMARY KEY (`version`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migration`
--

LOCK TABLES `migration` WRITE;
/*!40000 ALTER TABLE `migration`
    DISABLE KEYS */;
INSERT INTO `migration`
VALUES ('m000000_000000_base', 1579098992),
       ('m140506_102106_rbac_init', 1579099975),
       ('m170907_052038_rbac_add_index_on_auth_assignment_user_id', 1579099976),
       ('m180523_151638_rbac_updates_indexes_without_prefix', 1579099977),
       ('m200115_135933_create_objects_table', 1579099143),
       ('m200115_141612_create_users_table', 1579099144),
       ('m200115_233408_create_employees_table', 1579131524);
/*!40000 ALTER TABLE `migration`
    ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `objects`
--

DROP TABLE IF EXISTS `objects`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `objects`
(
    `id`   int(11) NOT NULL AUTO_INCREMENT,
    `name` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 37
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `objects`
--

LOCK TABLES `objects` WRITE;
/*!40000 ALTER TABLE `objects`
    DISABLE KEYS */;
INSERT INTO `objects`
VALUES (0, 'jhgjgjhg'),
       (1, 'nisi'),
       (2, 'unde'),
       (3, 'consectetur'),
       (4, 'quasi'),
       (5, 'illo'),
       (6, 'consequatur'),
       (7, 'laudantium'),
       (8, 'amet'),
       (9, 'et'),
       (10, 'quia'),
       (11, 'voluptatem'),
       (12, 'nihil'),
       (13, 'quaerat'),
       (14, 'aut'),
       (15, 'est'),
       (16, 'officia'),
       (17, 'fuga'),
       (18, 'expedita'),
       (19, 'eum'),
       (20, 'et'),
       (21, 'ab'),
       (22, 'qui'),
       (23, 'et'),
       (24, 'similique'),
       (25, 'rerum'),
       (26, 'aperiam'),
       (27, 'explicabo'),
       (28, 'dolor'),
       (29, 'itaque'),
       (30, 'neque'),
       (31, 'vero'),
       (32, 'optio'),
       (33, 'expedita'),
       (34, 'praesentium'),
       (35, 'eos');
/*!40000 ALTER TABLE `objects`
    ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users`
(
    `id`            int(11)                                 NOT NULL AUTO_INCREMENT,
    `username`      varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
    `password_hash` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
    `email`         varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
    `object_id`     int(11) DEFAULT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY `username` (`username`),
    UNIQUE KEY `email` (`email`),
    KEY `user_object_fk` (`object_id`),
    CONSTRAINT `user_object_fk` FOREIGN KEY (`object_id`) REFERENCES `objects` (`id`) ON DELETE SET NULL
) ENGINE = InnoDB
  AUTO_INCREMENT = 18
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users`
    DISABLE KEYS */;
INSERT INTO `users`
VALUES (1, 'admin', '$2y$13$wsIN2Pz5BVZ49c2Tjct3oe74R8Qompyx7nKomgbnt6l7QehnW1QD2', 'admin@admin.ad', NULL),
       (17, 'manager', '$2y$13$zvEFhyScGGUN1X12JbsD5.ZRH4gzPgUouc/ndeof0M09lRRkOB9Aq', 'dasdasd@wewewe.ew', 13);
/*!40000 ALTER TABLE `users`
    ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE = @OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE = @OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS = @OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS = @OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT = @OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS = @OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION = @OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES = @OLD_SQL_NOTES */;

-- Dump completed on 2020-01-16  3:02:27
