-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Янв 16 2020 г., 19:12
-- Версия сервера: 5.7.25-log
-- Версия PHP: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `tabel`
--

-- --------------------------------------------------------

--
-- Структура таблицы `auth_assignment`
--

CREATE TABLE `auth_assignment` (
  `item_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `auth_assignment`
--

INSERT INTO `auth_assignment` (`item_name`, `user_id`, `created_at`) VALUES
('admin', '1', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `auth_item`
--

CREATE TABLE `auth_item` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `type` smallint(6) NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `rule_name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` blob,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `auth_item`
--

INSERT INTO `auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`) VALUES
('admin', 1, NULL, NULL, NULL, NULL, NULL),
('manager', 1, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `auth_item_child`
--

CREATE TABLE `auth_item_child` (
  `parent` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `child` varchar(64) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `auth_rule`
--

CREATE TABLE `auth_rule` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `data` blob,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `employees`
--

CREATE TABLE `employees` (
  `id` int(11) NOT NULL,
  `object_id` int(11) DEFAULT NULL,
  `full_name` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `employees`
--

INSERT INTO `employees` (`id`, `object_id`, `full_name`, `is_active`) VALUES
(2, 8, 'Malcolm Effertz', 0),
(3, 2, 'Meda Schultz', 0),
(4, 1, 'Roselyn Barrows', 1),
(5, 9, 'Prof. Jarret Vandervort DDS', 0),
(6, 8, 'D\'angelo Beahan I', 0),
(8, 5, 'Melyna Rempel', 0),
(9, 7, 'Darien Roob IV', 0),
(10, 3, 'Erin Wilkinson', 0),
(12, 4, 'Jeanne Kilback', 1),
(13, 8, 'Amos Spencer', 0),
(14, 4, 'Prof. Jan Hand', 0),
(15, 9, 'Prof. Arvilla Watsica', 0),
(17, 9, 'Dillon O\'Reilly', 0),
(18, 8, 'Reanna Zulauf', 1),
(19, 2, 'Chris Turner', 1),
(20, 8, 'Pietro Rath', 0),
(21, 4, 'Deontae Mayer III', 0),
(22, 5, 'Ms. Patience Ward', 0),
(24, 1, 'Joelle Berge Jr.', 0),
(25, 1, 'Wilburn Batz', 1),
(26, 8, 'Darrion Stokes', 0),
(27, 7, 'Miss Thelma Leffler I', 1),
(28, 2, 'Phoebe Feest I', 0),
(29, 7, 'Ima Jaskolski', 1),
(30, 7, 'Dr. Travis Russel', 1),
(31, 7, 'Skylar Pacocha PhD', 1),
(32, 5, 'Naomie Wunsch III', 0),
(33, 6, 'Elian Kerluke', 0),
(34, 8, 'Kory Jerde', 1),
(35, 4, 'Susan Marvin', 0),
(36, 5, 'Kailyn Rutherford', 1),
(37, 9, 'Miss Aniyah O\'Kon', 0),
(38, 1, 'Jarod Spinka', 0),
(39, 3, 'Wendell Berge', 0),
(40, 7, 'Marge Dickens', 1),
(41, 8, 'Flo Wehner', 0),
(42, 2, 'Aditya Kassulke', 1),
(43, 4, 'Destiney Langworth', 1),
(44, 8, 'Mr. Kristopher Walter', 0),
(45, 9, 'Nella Harvey III', 1),
(47, 1, 'Prof. Clifton Mohr', 1),
(48, 1, 'Dr. Danny Jast Jr.', 1),
(49, 8, 'Gay Adams', 0),
(50, 6, 'Mikel Kulas', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `migration`
--

CREATE TABLE `migration` (
  `version` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1579098992),
('m140506_102106_rbac_init', 1579099975),
('m170907_052038_rbac_add_index_on_auth_assignment_user_id', 1579099976),
('m180523_151638_rbac_updates_indexes_without_prefix', 1579099977),
('m200115_135933_create_objects_table', 1579099143),
('m200115_141612_create_users_table', 1579099144),
('m200115_233408_create_employees_table', 1579181569),
('m200116_135940_create_records_table', 1579189473);

-- --------------------------------------------------------

--
-- Структура таблицы `objects`
--

CREATE TABLE `objects` (
  `id` int(11) NOT NULL,
  `name` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `objects`
--

INSERT INTO `objects` (`id`, `name`) VALUES
(1, 'nisi'),
(2, 'unde'),
(3, 'consectetur'),
(4, 'quasi'),
(5, 'illo'),
(6, 'consequatur'),
(7, 'laudantium'),
(8, 'amet'),
(9, 'et'),
(10, 'quia'),
(11, 'voluptatem'),
(12, 'nihil'),
(13, 'quaerat'),
(14, 'aut'),
(15, 'est'),
(16, 'officia'),
(17, 'fuga'),
(18, 'expedita'),
(19, 'eum'),
(20, 'et'),
(21, 'ab'),
(22, 'qui'),
(23, 'et'),
(24, 'similique'),
(25, 'rerum'),
(26, 'aperiam'),
(27, 'explicabo'),
(28, 'dolor'),
(29, 'itaque'),
(30, 'neque'),
(31, 'vero'),
(32, 'optio'),
(33, 'expedita'),
(34, 'praesentium'),
(35, 'eos'),
(999, 'Офис');

-- --------------------------------------------------------

--
-- Структура таблицы `records`
--

CREATE TABLE `records` (
  `id` int(11) NOT NULL,
  `created_at` int(11) NOT NULL,
  `object_id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `type` smallint(6) NOT NULL,
  `start` int(11) NOT NULL,
  `end` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `records`
--

INSERT INTO `records` (`id`, `created_at`, `object_id`, `employee_id`, `type`, `start`, `end`) VALUES
(1, 1579181400, 2, 28, 1, 1579087800, 1579107600);

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `object_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `username`, `password_hash`, `email`, `object_id`) VALUES
(1, 'admin', '$2y$13$wsIN2Pz5BVZ49c2Tjct3oe74R8Qompyx7nKomgbnt6l7QehnW1QD2', 'admin@admin.ad', 25);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `auth_assignment`
--
ALTER TABLE `auth_assignment`
  ADD PRIMARY KEY (`item_name`,`user_id`),
  ADD KEY `idx-auth_assignment-user_id` (`user_id`);

--
-- Индексы таблицы `auth_item`
--
ALTER TABLE `auth_item`
  ADD PRIMARY KEY (`name`),
  ADD KEY `rule_name` (`rule_name`),
  ADD KEY `idx-auth_item-type` (`type`);

--
-- Индексы таблицы `auth_item_child`
--
ALTER TABLE `auth_item_child`
  ADD PRIMARY KEY (`parent`,`child`),
  ADD KEY `child` (`child`);

--
-- Индексы таблицы `auth_rule`
--
ALTER TABLE `auth_rule`
  ADD PRIMARY KEY (`name`);

--
-- Индексы таблицы `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`id`),
  ADD KEY `employee_object_fk` (`object_id`);

--
-- Индексы таблицы `migration`
--
ALTER TABLE `migration`
  ADD PRIMARY KEY (`version`);

--
-- Индексы таблицы `objects`
--
ALTER TABLE `objects`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `records`
--
ALTER TABLE `records`
  ADD PRIMARY KEY (`id`),
  ADD KEY `record_employee_fk` (`employee_id`),
  ADD KEY `record_object_fk` (`object_id`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`),
  ADD KEY `user_object_fk` (`object_id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `employees`
--
ALTER TABLE `employees`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT для таблицы `objects`
--
ALTER TABLE `objects`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1000;

--
-- AUTO_INCREMENT для таблицы `records`
--
ALTER TABLE `records`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `auth_assignment`
--
ALTER TABLE `auth_assignment`
  ADD CONSTRAINT `auth_assignment_ibfk_1` FOREIGN KEY (`item_name`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `auth_item`
--
ALTER TABLE `auth_item`
  ADD CONSTRAINT `auth_item_ibfk_1` FOREIGN KEY (`rule_name`) REFERENCES `auth_rule` (`name`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `auth_item_child`
--
ALTER TABLE `auth_item_child`
  ADD CONSTRAINT `auth_item_child_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `auth_item_child_ibfk_2` FOREIGN KEY (`child`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `employees`
--
ALTER TABLE `employees`
  ADD CONSTRAINT `employee_object_fk` FOREIGN KEY (`object_id`) REFERENCES `objects` (`id`) ON DELETE SET NULL;

--
-- Ограничения внешнего ключа таблицы `records`
--
ALTER TABLE `records`
  ADD CONSTRAINT `record_employee_fk` FOREIGN KEY (`employee_id`) REFERENCES `employees` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `record_object_fk` FOREIGN KEY (`object_id`) REFERENCES `objects` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `user_object_fk` FOREIGN KEY (`object_id`) REFERENCES `objects` (`id`) ON DELETE SET NULL;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
