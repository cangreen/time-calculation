<?php

use kartik\datecontrol\Module;

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';

$config = [
  'id' => 'basic',
  'language' => 'ru',
  'basePath' => dirname(__DIR__),
  'bootstrap' => ['log'],
  'aliases' => [
    '@bower' => '@vendor/bower-asset',
    '@npm' => '@vendor/npm-asset',
  ],
  'components' => [
    'request' => [
      // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
      'cookieValidationKey' => 'sh_YgQVLAkxwDwu_kiB99-c5iK9CZEnf',
    ],
    'cache' => [
      'class' => 'yii\caching\FileCache',
    ],
    'user' => [
      'identityClass' => 'app\models\User',
      'enableAutoLogin' => true,
    ],
    'errorHandler' => [
      'errorAction' => 'site/error',
    ],
    'mailer' => [
      'class' => 'yii\swiftmailer\Mailer',
      // send all mails to a file by default. You have to set
      // 'useFileTransport' to false and configure a transport
      // for the mailer to send real emails.
      'useFileTransport' => true,
    ],
    'log' => [
      'traceLevel' => YII_DEBUG ? 3 : 0,
      'targets' => [
        [
          'class' => 'yii\log\FileTarget',
          'levels' => ['error', 'warning'],
        ],
      ],
    ],
    'db' => $db,
    'urlManager' => [
      'enablePrettyUrl' => true,
      'showScriptName' => false,
      'rules' => [
      ],
    ],
    'authManager' => [
      'class' => 'yii\rbac\DbManager',
    ],
    'formatter' => [
      'defaultTimeZone' => 'Europe/Moscow',
      'timeZone' => 'GMT+03:00',
      'dateFormat' => 'dd-MM-yyyy',
      'datetimeFormat' => 'php:d.m.Y H:i'
    ],
    'assetManager' => [
      'appendTimestamp' => true,
    ],
  ],
  'modules' => [
    'administrator' => [
      'class' => 'app\modules\administrator\Module',
    ],
    'manager' => [
      'class' => 'app\modules\manager\Module',
    ],
    'datecontrol' => [
      'class' => 'kartik\datecontrol\Module',

      // format settings for displaying each date attribute (ICU format example)
      'displaySettings' => [
        Module::FORMAT_DATE => 'dd-MM-yyyy',
        Module::FORMAT_TIME => 'HH:mm:ss a',
        Module::FORMAT_DATETIME => 'dd-MM-yyyy HH:mm',
      ],

      // format settings for saving each date attribute (PHP format example)
      'saveSettings' => [
        Module::FORMAT_DATE => 'php:U', // saves as unix timestamp
        Module::FORMAT_TIME => 'php:H:i:s',
        Module::FORMAT_DATETIME => 'php:U',
      ],
      'displayTimezone' => 'GMT+03:00',
      'saveTimezone' => 'GMT+03:00',
    ],
    'gridview' => [
      'class' => '\kartik\grid\Module'
    ]
  ],
  'params' => $params,
];

if (YII_ENV_DEV) {
  // configuration adjustments for 'dev' environment
  $config['bootstrap'][] = 'debug';
  $config['modules']['debug'] = [
    'class' => 'yii\debug\Module',
    // uncomment the following to add your IP if you are not connecting from localhost.
    //'allowedIPs' => ['127.0.0.1', '::1'],
  ];

  $config['bootstrap'][] = 'gii';
  $config['modules']['gii'] = [
    'class' => 'yii\gii\Module',
    'generators' => [
      'crud' => [
        'class' => 'yii\gii\generators\crud\Generator',
        'templates' => [
          'adminlte' => '@vendor/dmstr/yii2-adminlte-asset/gii/templates/crud/simple',
          'adminlteRus' => '@app/generators/gii/templates/crud/simple',
        ]
      ]
    ],
    // uncomment the following to add your IP if you are not connecting from localhost.
    //'allowedIPs' => ['127.0.0.1', '::1'],
  ];
}

return $config;
