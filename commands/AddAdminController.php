<?php

namespace app\commands;

use app\models\CreateUser;
use app\models\Users;
use yii\console\Controller;
use yii\console\ExitCode;


class AddAdminController extends Controller
{
  public function actionIndex()
  {
    $user = new Users();
    $user->email = 'admin@admin.ad';
    $user->username = 'admin';
    $user->setPassword('PMU8nrOr');
    if ($user->save()) {
      echo 'admin created' . PHP_EOL;
    }
    $this->addRole($user->id);
  }

  private function addRole($id)
  {
   $am = \Yii::$app->authManager;
   $role = $am->getRole('admin');
   if($am->assign($role, $id)){
     echo 'role assigned' . PHP_EOL;
   };
  }
}
